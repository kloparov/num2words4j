/**
 * BulgarianDictionary.java
 *
 * $Revision: $
 * $LastChangedBy: $
 *
 * $LastChangedDate: $
 *
 * Copyright (c) Unicredit Factoring. All rights reserved.
 * Unicredit Factoring PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package bg.num2words4j.dictionary;

import java.util.HashMap;
import java.util.Map;

import bg.num2words4j.core.Language;


/**
 * @author kloparov
 *
 */
public class BulgarianDictionary implements Dictionary {
	public static final Language language = Language.BG;
	public static final String AND = "и ";
	public static final String HUNDRED = " сто ";
	public static final String HUNDREDS_A = "ста ";
	public static final String HUNDREDS_I = "стотин ";
	public static final String MILION = " милион ";
	public static final String MILIONS = " милиона ";
	public static final String THOUSAND = " хиляда ";
	public static final String THOUSANDS = " хиляди ";
	
	public static final String AND_ONE = "и една";
	public static final String ONE_MILION = " един" + MILION;
	public static final String TWO_MILIONS = " два" + MILIONS;
		
	public static final Map<Integer, String> FROM_ZERO_TO_NIHHTY_NIGHT = new HashMap<Integer, String>() {
		{
			put(0, "нула ");
			put(1, "едно ");
			put(2, "две ");
			put(3, "три ");
			put(4, "четири ");
			put(5, "пет ");
			put(6, "шест ");
			put(7, "седем ");
			put(8, "осем ");
			put(9, "девет ");
			put(10, "десет ");
			put(11, "единадесет ");
			put(12, "дванадесет ");
			put(13, "тринадесет ");
			put(14, "четиринадесет ");
			put(15, "петнадесет ");
			put(16, "шестнадесет ");
			put(17, "седемнадесет ");
			put(18, "осемнадесет ");
			put(19, "деветнадесет ");
			put(20, "двайсет ");
			put(21, "двайсет и едно ");
			put(22, "двайсет и две ");
			put(23, "двайсет и три ");
			put(24, "двайсет и четири ");
			put(25, "двайсет и пет ");
			put(26, "двайсет и шест ");
			put(27, "двайсет и седем ");
			put(28, "двайсет и осем ");
			put(29, "двайсет и девет ");
			put(30, "тридесет ");
			put(31, "тридесет и едно ");
			put(32, "тридесет и две ");
			put(33, "тридесет и три ");
			put(34, "тридесет и четири ");
			put(35, "тридесет и пет ");
			put(36, "тридесет и шест ");
			put(37, "тридесет и седем ");
			put(38, "тридесет и осем ");
			put(39, "тридесет и девет ");
			put(40, "четирдесет ");
			put(41, "четирдесет и едно ");
			put(42, "четирдесет и две ");
			put(43, "четирдесет и три ");
			put(44, "четирдесет и четири ");
			put(45, "четирдесет и пет ");
			put(46, "четирдесет и шест ");
			put(47, "четирдесет и седем ");
			put(48, "четирдесет и осем ");
			put(49, "четирдесет и девет ");
			put(50, "петдесет ");
			put(51, "петдесет и едно ");
			put(52, "петдесет и две ");
			put(53, "петдесет и три ");
			put(54, "петдесет и четири ");
			put(55, "петдесет и пет ");
			put(56, "петдесет и шест ");
			put(57, "петдесет и седем ");
			put(58, "петдесет и осем ");
			put(59, "петдесет и девет ");
			put(60, "шейсет ");
			put(61, "шейсет и едно ");
			put(62, "шейсет и две ");
			put(63, "шейсет и три ");
			put(64, "шейсет и четири ");
			put(65, "шейсет и пет ");
			put(66, "шейсет и шест ");
			put(67, "шейсет и седем ");
			put(68, "шейсет и осем ");
			put(69, "шейсет и девет ");
			put(70, "седемдесет ");
			put(71, "седемдесет и едно ");
			put(72, "седемдесет и две ");
			put(73, "седемдесет и три ");
			put(74, "седемдесет и четири ");
			put(75, "седемдесет и пет ");
			put(76, "седемдесет и шест ");
			put(77, "седемдесет и седем ");
			put(78, "седемдесет и осем ");
			put(79, "седемдесет и девет ");
			put(80, "осемдесет ");
			put(81, "осемдесет и едно ");
			put(82, "осемдесет и две ");
			put(83, "осемдесет и три ");
			put(84, "осемдесет и четири ");
			put(85, "осемдесет и пет ");
			put(86, "осемдесет и шест ");
			put(87, "осемдесет и седем ");
			put(88, "осемдесет и осем ");
			put(89, "осемдесет и девет ");
			put(90, "деведесет ");
			put(91, "деведесет и едно ");
			put(92, "деведесет и две ");
			put(93, "деведесет и три ");
			put(94, "деведесет и четири ");
			put(95, "деведесет и пет ");
			put(96, "деведесет и шест ");
			put(97, "деведесет и седем ");
			put(98, "деведесет и осем ");
			put(99, "деведесет и девет ");
		}
	};
	
	@Override
	public String getAnd() {
		return AND;
	}

	@Override
	public String getHundred() {
		return HUNDRED;
	}

	@Override
	public String getHundreds() {
		throw new RuntimeException("This is not supported for Bulgarian Language ! Please use  getHundreds_a or getHundreds_i");
	}

	@Override
	public String getHundreds_a() {
		return HUNDREDS_A;
	}

	@Override
	public String getHundreds_i() {
		return HUNDREDS_I;
	}

	@Override
	public String getMilion() {
		return MILION;
	}

	@Override
	public String getMilions() {
		return MILIONS;
	}

	@Override
	public String getThousand() {
		return THOUSAND;
	}

	@Override
	public String getThousands() {
		return THOUSANDS;
	}

	@Override
	public String getOneMilion() {
		return ONE_MILION;
	}

	@Override
	public String getTwoMilions() {
		return TWO_MILIONS;
	}

	@Override
	public Map<Integer, String> getFromZeroToNightyNight() {
		return FROM_ZERO_TO_NIHHTY_NIGHT;
	}
	
	@Override
	public Language getLanguage() {
		return language;
	}
	
	@Override
	public String getAndOne() {
		return AND_ONE;
	}

}
