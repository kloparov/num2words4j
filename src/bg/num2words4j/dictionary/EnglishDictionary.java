/**
 * EnglishDictionary.java
 *
 * $Revision: $
 * $LastChangedBy: $
 *
 * $LastChangedDate: $
 *
 * Copyright (c) Unicredit Factoring. All rights reserved.
 * Unicredit Factoring PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package bg.num2words4j.dictionary;

import java.util.HashMap;
import java.util.Map;

import bg.num2words4j.core.Language;


/**
 * @author kloparov
 *
 */
public class EnglishDictionary implements Dictionary {
	public static final Language language = Language.EN;
	public static final String AND = " and ";
	public static final String HUNDRED = " hundred ";
	public static final String HUNDREDS = " hundreds ";
	public static final String MILION = " milion ";
	public static final String MILIONS = " milions ";
	public static final String THOUSAND = " thousand ";
	public static final String THOUSANDS = " thousands ";
		
	public static final Map<Integer, String> FROM_ZERO_TO_NIHHTY_NIGHT = new HashMap<Integer, String>(){
		{
			put(0, " zero ");
			put(1, " one ");
			put(2, " two ");
			put(3, " three ");
			put(4, " four ");
			put(5, " five ");
			put(6, " six ");
			put(7, " seven ");
			put(8, " eight ");
			put(9, " nine ");
			put(10, " ten ");
			put(11, " eleven ");
			put(12, " twelve ");
			put(13, " thirteen ");
			put(14, " fourteen ");
			put(15, " fifteen ");
			put(16, " sixteen ");
			put(17, " seventeen ");
			put(18, " nineteen ");
			put(19, " деветнадесет ");
			put(20, " twenty ");
			put(21, " twenty-one ");
			put(22, " twenty-two ");
			put(23, " twenty-three ");
			put(24, " twenty-four ");
			put(25, " twenty-five ");
			put(26, " twenty-six ");
			put(27, " twenty-seven ");
			put(28, " twenty-eight ");
			put(29, " twenty-nine ");
			put(30, " thirty ");
			put(31, " thirty-one ");
			put(32, " thirty-two ");
			put(33, " thirty-three ");
			put(34, " thirty-four ");
			put(35, " thirty-five ");
			put(36, " thirty-six ");
			put(37, " thirty-seven ");
			put(38, " thirty-eight ");
			put(39, " thirty-nine ");
			put(40, " fourty ");
			put(41, " fourty-one ");
			put(42, " fourty-two ");
			put(43, " fourty-three ");
			put(44, " fourty-four ");
			put(45, " fourty-five ");
			put(46, " fourty-six ");
			put(47, " fourty-seven ");
			put(48, " fourty-eight ");
			put(49, " fourty-nine ");
			put(50, " fifty ");
			put(51, " fifty-one ");
			put(52, " fifty-two ");
			put(53, " fifty-three ");
			put(54, " fifty-four ");
			put(55, " fifty-five ");
			put(56, " fifty-six ");
			put(57, " fifty-seven ");
			put(58, " fifty-eight ");
			put(59, " fifty-nine ");
			put(60, " sixty ");
			put(61, " sixty-one ");
			put(62, " sixty-two ");
			put(63, " sixty-three ");
			put(64, " sixty-four ");
			put(65, " sixty-five ");
			put(66, " sixty-six ");
			put(67, " sixty-seven ");
			put(68, " sixty-eight ");
			put(69, " sixty-nine ");
			put(70, " seventy ");
			put(71, " seventy-one ");
			put(72, " seventy-two ");
			put(73, " seventy-three ");
			put(74, " seventy-four ");
			put(75, " seventy-five ");
			put(76, " seventy-six ");
			put(77, " seventy-seven ");
			put(78, " seventy-eight ");
			put(79, " seventy-nine ");
			put(80, " eighty ");
			put(81, " eighty-one ");
			put(82, " eighty-two ");
			put(83, " eighty-three ");
			put(84, " eighty-four ");
			put(85, " eighty-five ");
			put(86, " eighty-six ");
			put(87, " eighty-seven ");
			put(88, " eighty-eight ");
			put(89, " eighty-nine ");
			put(90, " ninety ");
			put(91, " ninety-one ");
			put(92, " ninety-two ");
			put(93, " ninety-three ");
			put(94, " ninety-four ");
			put(95, " ninety-five ");
			put(96, " ninety-six ");
			put(97, " ninety-seven ");
			put(98, " ninety-eight ");
			put(99, " ninety-nine ");
		}
	};
	
	@Override
	public String getAnd() {
		return AND;
	}

	@Override
	public String getHundred() {
		return HUNDRED;
	}

	@Override
	public String getHundreds() {
		return HUNDREDS;
	}

	@Override
	public String getHundreds_a() {
		throw new RuntimeException("This is not supported for English Language ! Please use  getHundreds");
	}


	@Override
	public String getHundreds_i() {
		throw new RuntimeException("This is not supported for English Language ! Please use  getHundreds");
	}
	
	@Override
	public String getMilion() {
		return MILION;
	}

	@Override
	public String getMilions() {
		return MILIONS;
	}

	@Override
	public String getThousand() {
		return THOUSAND;
	}

	@Override
	public String getThousands() {
		return THOUSANDS;
	}

	@Override
	public String getOneMilion() {
		throw new RuntimeException("This is not supported for English Language ! Please use  getMilion");
	}

	@Override
	public String getTwoMilions() {
		throw new RuntimeException("This is not supported for English Language ! Please use  getMilion");
	}

	@Override
	public Map<Integer, String> getFromZeroToNightyNight() {
		return FROM_ZERO_TO_NIHHTY_NIGHT;
	}

	@Override
	public Language getLanguage() {
		return language;
	}
	
	@Override
	public String getAndOne() {
		throw new RuntimeException("This is not supported for English Language !");
	}

}
