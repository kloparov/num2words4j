/**
 * Dictionary.java
 *
 * $Revision: $
 * $LastChangedBy: $
 *
 * $LastChangedDate: $
 *
 * Copyright (c) Unicredit Factoring. All rights reserved.
 * Unicredit Factoring PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package bg.num2words4j.dictionary;

import java.util.Map;

import bg.num2words4j.core.Language;


/**
 * @author kloparov
 *
 */
public interface Dictionary {
	
	public String getAnd();
	
	public String getHundred();
	
	public String getHundreds();
	
	/** Bulgarian only */
	public String getHundreds_a();
	
	/** Bulgarian only */
	public String getHundreds_i();
	
	public String getMilion();
	
	public String getMilions();
	
	public String getThousand();
	
	public String getThousands();
	
	/** Bulgarian only */
	public String getOneMilion();
	
	/** Bulgarian only */
	public String getTwoMilions();
	
	public Map<Integer, String> getFromZeroToNightyNight();
	
	public Language getLanguage();
	
	public String getAndOne();
}
