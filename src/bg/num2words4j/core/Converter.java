/**
 * Converter.java
 *
 * $Revision: $
 * $LastChangedBy: $
 *
 * $LastChangedDate: $
 *
 * Copyright (c) Unicredit Factoring. All rights reserved.
 * Unicredit Factoring PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package bg.num2words4j.core;

import org.apache.commons.lang.StringUtils;

import bg.num2words4j.dictionary.Dictionary;

/**
 * @author kloparov
 *
 */
public class Converter {
	private static final String ZERO = "000";
	
	private Converter() {}

	/** convert the first part of number before demical point 
	 * example :
	 * src = 100 
	 * return = one hundred milions
	 * */
	private static void convertPartOne(String src, StringBuilder builder, Dictionary language) {
		if (!src.equals(ZERO)) {
			int one = Integer.parseInt(src.substring(0, 1));
			int two = Integer.parseInt(src.substring(1, 2));
			int three = Integer.parseInt(src.substring(2, 3));
			int twoAndThree = Integer.parseInt(src.substring(1,3));
			
			if (language.getLanguage() == Language.BG) {
				if (one != 0) {
					if (one == 1) {
						builder.append(language.getHundred());
					} else if (one > 1 && one < 3) {
						builder.append(language.getFromZeroToNightyNight().get(one).trim()).append(language.getHundreds_a());
						
					} else if (one > 3) {
						builder.append(language.getFromZeroToNightyNight().get(one).trim()).append(language.getHundreds_i());
					}
					
					if (twoAndThree == 0) {
						builder.append(language.getMilions());
					} 					
				}
				
				if (two != 0) {
					builder.append(language.getFromZeroToNightyNight().get(twoAndThree)).append(language.getMilions());
				}
				
				if (two == 0 && three != 0) {
					if (three == 1) {
						builder.append(language.getOneMilion());
					} else if (three == 2) {
						builder.append(language.getTwoMilions());
					} else if (three > 2) {
						builder.append(language.getFromZeroToNightyNight().get(three)).append(language.getMilions());
					}
				}
			} else {
				if (one != 0) {
					builder.append(language.getFromZeroToNightyNight().get(one)).append(language.getHundred());
				}
					
				if (two != 0) {
					builder.append(language.getAnd());
					builder.append(language.getFromZeroToNightyNight().get(twoAndThree)).append(language.getMilions());
				}
					
				if (two == 0 && three != 0) {
					builder.append(language.getFromZeroToNightyNight().get(three)).append(language.getMilions());
				}	
			}			
		}
	}
	
	/** return second part of number before demical point 
	 * example :
	 * src = 100
	 * return = one hundred thousand
	 * */
	private static void convertPartTwo(String src, StringBuilder builder, Dictionary language) {
		if (!src.equals(ZERO)) {
			int one = Integer.parseInt(src.substring(0, 1));
			int two = Integer.parseInt(src.substring(1, 2));
			int three = Integer.parseInt(src.substring(2, 3));
			int twoAndThree = Integer.parseInt(src.substring(1,3));
			
			if (language.getLanguage() == Language.BG) {
				if (one != 0) {
					if (one == 1) {
						builder.append(language.getHundred());
					} else if (one > 1 && one < 3) {
						builder.append(language.getFromZeroToNightyNight().get(one).trim()).append(language.getHundreds_a());
					} else if (one > 3) {
						builder.append(language.getFromZeroToNightyNight().get(one).trim()).append(language.getHundreds_i());
					}
				}
						
				if (two == 0 && three != 0) {
					if(three == 1) {
						builder.append(language.getThousand());
					} else if (three > 1) {
						builder.append(language.getFromZeroToNightyNight().get(three)).append(language.getThousands());
					}
	 			} else if (two != 0) {
	 				if (two > 1 && three == 1) {
	 					builder.append(language.getFromZeroToNightyNight().get(two * 10)).append(language.getAndOne()).append(language.getThousands());
	 				} else {
	 					builder.append(language.getFromZeroToNightyNight().get(twoAndThree)).append(language.getThousands());
	 				}
	 			} 
			} else {
				if (one != 0){
					if (one < 10) {
						builder.append(language.getFromZeroToNightyNight().get(one)).append(language.getHundred());
					} else {
						builder.append(language.getFromZeroToNightyNight().get(one)).append(language.getHundreds());
					}
				}
				
				if (two != 0) {
					builder.append(language.getFromZeroToNightyNight().get(twoAndThree)).append(language.getThousand());
				}
				
				if (two == 0 && three != 0) {
					builder.append(language.getFromZeroToNightyNight().get(three)).append(language.getThousand());
				}
			}
		}
	}
	
	/** return third part of number before demical point 
	 * example :
	 * src = 100
	 * return = one hundred
	 * */
	private static void convertPartThree(String src, StringBuilder builder, Dictionary language) {
		if (!src.equals(ZERO)) {
			int one = Integer.parseInt(src.substring(0, 1));
			int two = Integer.parseInt(src.substring(1, 2));
			int three = Integer.parseInt(src.substring(2, 3));
			int twoAndThree = Integer.parseInt(src.substring(1,3));
			
			if (language.getLanguage() == Language.BG) {
				if (one != 0) {
					if (one == 1) {
						builder.append(language.getHundred());
					} else if (one > 1 && one < 3) {
						builder.append(language.getFromZeroToNightyNight().get(one).trim()).append(language.getHundreds_a());
					} else if (one > 3) {
						builder.append(language.getFromZeroToNightyNight().get(one).trim()).append(language.getHundreds_i());
					}
				}
				
				if (two != 0) {
					builder.append(language.getFromZeroToNightyNight().get(twoAndThree));
				}
				
				if (two == 0 && three != 0) {
					builder.append(language.getFromZeroToNightyNight().get(three));
				}
			} else {
				if (one != 0) {
					builder.append(language.getFromZeroToNightyNight().get(one)).append(language.getHundred());
				}
				
				if (two != 0) {
					builder.append(language.getFromZeroToNightyNight().get(twoAndThree));
				}
				
				if (two == 0 && three != 0) {
					builder.append(language.getFromZeroToNightyNight().get(three));
				}
			}
		}
	}
	
	/** if src is less than 9 character than 0 is appended at the beginning of the src
	 * example :
	 *  src = 999;
	 *  return = 000000999
	 * 
	 * */
	private static String formatBeforeDemicalDot(String src) {
		if(src.length() < 10){
			src = String.format("%09d", Integer.valueOf(src));
		}
		return src; 
	}
	
	/** if src is less than 9 character than 0 is appended at the beginning of the src
	 * example :
	 *  src = 99;
	 *  return = 009
	 * 
	 * */
	private static String formatAfterDemicalDot(String src) {
		if(src.length() < 4){
			for (int i = 0; i <src.length(); i++ ) {
				if (src.length() == 3) {
					return src;
				}
				src = src.concat("0");
			}
		}
		return src; 
	}
	
	/** return String representaion of number before demical point 
	 * example :
	 * src = 9.99
	 * return = nine
	 *  */
	public static String convertBeforeDemicalPoint(String src, Dictionary language) {
		src = formatBeforeDemicalDot(src);
		StringBuilder output = new StringBuilder();
		StringBuilder partOneBuider = new StringBuilder();
		StringBuilder partTwoBuilder = new StringBuilder();
		StringBuilder partThreeBuilder = new StringBuilder();
		
		String partOne = src.substring(0, 3);
		String partTwo = src.substring(3, 6);
		String partThree = src.substring(6, 9);
		
		convertPartOne(partOne, partOneBuider, language);
		convertPartTwo(partTwo, partTwoBuilder, language);
		convertPartThree(partThree, partThreeBuilder, language);
		
		boolean isZero = (partOneBuider.length() == 0 && partTwoBuilder.length() == 0 && partThreeBuilder.length() == 0) ? true : false;
		output.append((isZero) ? language.getFromZeroToNightyNight().get(0) : partOneBuider.toString()).append(partTwoBuilder.toString()).append(partThreeBuilder);
		return output.toString();
	}
	
	/** return String representaion of number afeter demical point 
	 * example :
	 * src = 9.99
	 * return = 0.99
	 *  */
	private static String convertAfertDemicalPoint(String src, Dictionary language) {
		StringBuilder builder = new StringBuilder();
		if (src != null && !src.equals(ZERO)) { 
			src = formatAfterDemicalDot(src);
			builder.append(language.getAnd());
			int one = Integer.parseInt(src.substring(0, 1));
			int three = Integer.parseInt(src.substring(2, 3));
			int oneAndTwo = Integer.parseInt(src.substring(0, 2));
			
			if (language.getLanguage() == Language.BG) {
				if (one != 0 && oneAndTwo != 0) {
					builder.append("0.");
					if (three < 5) {
						builder.append(oneAndTwo);
					} else {
						oneAndTwo = oneAndTwo + 1;
						builder.append(oneAndTwo);
					}
				} else if (one == 0) {
					builder.append("0.0");
					if (three < 5) {
						builder.append(oneAndTwo);
					} else {
						oneAndTwo = oneAndTwo + 1;
						builder.append(oneAndTwo);
					}
				}
			} else {
				if (one != 0 && oneAndTwo != 0) {
					if (three < 5) {
						builder.append(language.getFromZeroToNightyNight().get(oneAndTwo));
					} else {
						oneAndTwo = oneAndTwo + 1;
						builder.append(language.getFromZeroToNightyNight().get(oneAndTwo));
					}
				} else if (one == 0) {
					if (three < 5) {
						builder.append(language.getFromZeroToNightyNight().get(oneAndTwo));
					} else {
						oneAndTwo = oneAndTwo + 1;
						builder.append(language.getFromZeroToNightyNight().get(oneAndTwo));
					}
				}
			}
		} 
		return builder.toString();
	}
	
	/** return String representation of the number 
	 * example :
	 * src = 99.99
	 * return = ninty-nine and 0.99
	 * */
	public static String toWords(String beforeDot, String afterDot, Dictionary language) {
		StringBuilder toWords = new StringBuilder().append(convertBeforeDemicalPoint(beforeDot, language))
				.append(convertAfertDemicalPoint(afterDot, language));
		return toWords.toString();
	}
	
	public static String toWords(String beforeDot, String currencyCode, String afterDot, String cents, Dictionary language) {
		StringBuilder toWords = new StringBuilder().append(convertBeforeDemicalPoint(beforeDot, language)).append(currencyCode)
				.append(convertAfertDemicalPoint(afterDot, language)).append((StringUtils.isBlank(cents) == false) ? "" : cents);
		return toWords.toString();
	}
	
}
