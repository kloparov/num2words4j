/**
 * NumbersToWords.java
 *
 * $Revision: $
 * $LastChangedBy: $
 *
 * $LastChangedDate: $
 *
 * Copyright (c) Unicredit Factoring. All rights reserved.
 * Unicredit Factoring PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package bg.num2words4j.core;

import java.math.BigDecimal;

import javax.print.attribute.standard.MediaSize.Other;

import org.apache.commons.lang.StringUtils;

import bg.num2words4j.dictionary.BulgarianDictionary;
import bg.num2words4j.dictionary.Dictionary;
import bg.num2words4j.dictionary.EnglishDictionary;


/**
 * @author kloparov
 *
 */
public class NumbersToWords {
	private NumbersToWords() {}
	
	private static String splitAndConvert(String src, Dictionary lang) {
		if (src == null || lang == null) 
			return null;
		
		String[] content = split(src);
 		return Converter.toWords(content[0], content[1], lang);
	}
	
	private static String[] split(String src) {
		String[] output = new String[2];
 		String[] content = src.toString().split("\\.");
		output[0]= content[0];
		output[1] = (content.length > 1) ? content[1] : null;
		return output;
	}
	
	public static String toWords(BigDecimal src, Dictionary language) {
		return splitAndConvert(src.toString(), language);
	}
	
	public static String toWords(String src, Dictionary language) {
		return splitAndConvert(src.toString(), language);
	}
	
	public static String toWords(double src, Dictionary language) {
		return splitAndConvert(String.valueOf(src), language);
	}
	
	public static String toWords(int src, Dictionary language) {
		return splitAndConvert(String.valueOf(src), language);
	}
	
	public static String toWords(long src, Dictionary language) {
		return splitAndConvert(String.valueOf(src), language);
	}
	
	public static String toWords(short src, Dictionary language) {
		return splitAndConvert(String.valueOf(src), language);
	}
	
	public static String toWords(float src, Dictionary language) {
		return splitAndConvert(String.valueOf(src), language);
	}
	
	public static String toWords(Integer src, Dictionary language) {
		return splitAndConvert(src.toString(), language);
	}
	
	public static String toWords(Double src, Dictionary language) {
		return splitAndConvert(src.toString(), language);
	}
	
	public static String toWords(Short src, Dictionary language) {
		return splitAndConvert(src.toString(), language);
	}
	
	public static String toWords(Long src, Dictionary language) {
		return splitAndConvert(src.toString(), language);
	}
	
	public static String toWords(Float src, Dictionary language) {
		return splitAndConvert(src.toString(), language);
	}
}
