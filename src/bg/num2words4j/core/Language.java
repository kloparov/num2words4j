/**
 * Language.java
 *
 * $Revision: $
 * $LastChangedBy: $
 *
 * $LastChangedDate: $
 *
 * Copyright (c) Unicredit Factoring. All rights reserved.
 * Unicredit Factoring PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package bg.num2words4j.core;

/**
 * @author kloparov
 *
 */
public enum Language {
	BG, EN;
}
